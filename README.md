# PHILIPP PLEIN test app

Use docker and make for project testing. You can use 3 commands:

- make build
- make up
- make down

## Instructions

1. composer update
2. make build
3. make up
4. docker-compose run --rm pp-php-cli php bin/console doctrine:migrations:migrate
5. Website will be available here http://localhost:8080/ 

## Project contains

- 2 entity: Product and ShowOrder (ShowOrder instead of Order because "order" is reserved PostgreSQL name)
- 1 migration
- ApiController for emulating external API

## API emulating

- http://localhost:8080/api/orders_search
- http://localhost:8080/api/orders/1 (/api/orders/{id})

## Update local orders DB

There are 2 ways to update local orders DB:
1. Request API every time order list (homepage) is request by user. This method is less faster, but provides realtime information. This method is provided in **App\Controller\HomeController**
2. Update, using cronjob and php console command script. This method is faster, because we don't need to wait for API response. This method is provided by **App\Command\SyncOrdersCommand** class.

## Sync with remote API

We know, that API contains 2 methods: **POST orders_search** and **GET orders**.

Interaction with API is implemented in **App\Service\TestApiOrderSynchronizer** class, which implements **OrderSynchronizerInterface** the interface.

**OrderSynchronizerInterface** contains 2 methods: one method for one API endpoint.

There is hardcode in **App\Service\TestApiOrderSynchronizer** constructor: as we haven't got real API and forced to emulate it in same application, API url is hardcoded to SERVER_ADDR address ($_SERVER['SERVER_ADDR']). This will not work on **cli**, so change it to something else if you want to use cli. 

