<?php


namespace App\Model\DTO;

/**
 * Product data transfer class
 * Class Product
 * @package App\Model\DTO
 */
class Product
{
    public $barcode;
    public $price;

    /**
     * Product constructor.
     * @param $barcode
     * @param $price
     */
    public function __construct($barcode, $price)
    {
        $this->barcode = $barcode;
        $this->price = $price;
    }
}