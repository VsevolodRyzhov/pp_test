<?php


namespace App\Model\DTO;

/**
 * ShowOrder data transfer class
 * Class ShowOrder
 * @package App\Model\DTO
 */
class ShopOrder
{
    public $orderId;
    public $phone;
    public $shipping_status;
    public $shipping_price;
    public $shipping_payment_status;
    public $payment_status;
    public $created_at;

    private $products = [];

    /**
     * ShopOrder constructor.
     * @param $orderId
     * @param $phone
     * @param $shipping_status
     * @param $shipping_price
     * @param $shipping_payment_status
     * @param $payment_status
     * @param $created_at
     */
    public function __construct($orderId, $phone, $shipping_status, $shipping_price, $shipping_payment_status, $payment_status, $created_at)
    {
        $this->orderId = $orderId;
        $this->phone = $phone;
        $this->shipping_status = $shipping_status;
        $this->shipping_price = $shipping_price;
        $this->shipping_payment_status = $shipping_payment_status;
        $this->payment_status = $payment_status;
        $this->created_at = $created_at;
    }

    /**
     * @return ShopOrderProduct[]
     */
    public function getProducts(): ?iterable
    {
        return $this->products;
    }

    public function addProduct(ShopOrderProduct $product)
    {
        $this->products[] = $product;
    }
}