<?php


namespace App\Model\DTO;

/**
 * ShopOrderProduct data transfer class
 * Class ShopOrderProduct
 * @package App\Model\DTO
 */
class ShopOrderProduct
{
    public $order;
    public $product;

    public $cost;
    public $tax_perc;
    public $tax_amt;
    public $quantity;
    public $tracking_number;
    public $canceled;
    public $shipped_status_sku;

    /**
     * ShopOrderProduct constructor.
     * @param $cost
     * @param $tax_perc
     * @param $tax_amt
     * @param $quantity
     * @param $tracking_number
     * @param $canceled
     * @param $shipped_status_sku
     */
    public function __construct($cost, $tax_perc, $tax_amt, $quantity, $tracking_number, $canceled, $shipped_status_sku)
    {
        $this->cost = $cost;
        $this->tax_perc = $tax_perc;
        $this->tax_amt = $tax_amt;
        $this->quantity = $quantity;
        $this->tracking_number = $tracking_number;
        $this->canceled = $canceled;
        $this->shipped_status_sku = $shipped_status_sku;
    }
}