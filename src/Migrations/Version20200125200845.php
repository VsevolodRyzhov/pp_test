<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200125200845 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE product_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE shop_order_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE product (id INT NOT NULL, barcode BIGINT NOT NULL, price NUMERIC(10, 0) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE shop_order_product (shop_order_id INT NOT NULL, product_id INT NOT NULL, cost NUMERIC(10, 0) NOT NULL, tax_perc SMALLINT NOT NULL, tax_amt NUMERIC(10, 0) NOT NULL, quantity SMALLINT NOT NULL, tracking_number VARCHAR(255) NOT NULL, canceled VARCHAR(1) NOT NULL, shipped_status_sku VARCHAR(16) NOT NULL, PRIMARY KEY(shop_order_id, product_id))');
        $this->addSql('CREATE INDEX IDX_3B2C3AE9562797AE ON shop_order_product (shop_order_id)');
        $this->addSql('CREATE INDEX IDX_3B2C3AE94584665A ON shop_order_product (product_id)');
        $this->addSql('CREATE TABLE shop_order (id INT NOT NULL, order_id VARCHAR(16) NOT NULL, phone VARCHAR(16) DEFAULT NULL, shipping_status VARCHAR(16) DEFAULT NULL, shipping_price NUMERIC(10, 0) DEFAULT NULL, shipping_payment_status VARCHAR(16) DEFAULT NULL, payment_status VARCHAR(16) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE shop_order_product ADD CONSTRAINT FK_3B2C3AE9562797AE FOREIGN KEY (shop_order_id) REFERENCES shop_order (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE shop_order_product ADD CONSTRAINT FK_3B2C3AE94584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE shop_order_product DROP CONSTRAINT FK_3B2C3AE94584665A');
        $this->addSql('ALTER TABLE shop_order_product DROP CONSTRAINT FK_3B2C3AE9562797AE');
        $this->addSql('DROP SEQUENCE product_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE shop_order_id_seq CASCADE');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE shop_order_product');
        $this->addSql('DROP TABLE shop_order');
    }
}
