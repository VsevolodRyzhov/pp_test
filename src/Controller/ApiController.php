<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/orders_search", name="orders_search", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function ordersSearch(Request $request)
    {
        $count = $request->get('count', 20);
        $date_from = $request->get('date_from', date('Y-m-d'));
        $date_to = $request->get('date_to', date('Y-m-d'));
        $list = $this->emulateDbQuery();
        $ret = [];
        for ($i = 0; $i < $count && count($list) > 0; $i++) {
            $ret[] = array_shift($list);
        }
        return new JsonResponse($ret);
    }

    /**
     * @Route("/api/orders/{order_id}", name="orders", methods={"GET"})
     * @return JsonResponse
     */
    public function orders($order_id)
    {
        $data = $this->emulateDbQuery();
        foreach ($data as $order) {
            if ($order['orderId'] === $order_id) {
                $response = new JsonResponse($order);
                return $response;
            }
        }

        throw $this->createNotFoundException();
    }

    private function emulateDbQuery()
    {
        return [
            [
                "orderItems" => [
                    [
                        "barcode" => "4062345021658",
                        "price" => 24300,
                        "cost" => 24300,
                        "tax_perc" => 20,
                        "tax_amt" => 4050,
                        "quantity" => 1,
                        "tracking_number" => "1Z05V36Y7951053268",
                        "canceled" => "N",
                        "shipped_status_sku" => "not sent"
                    ],
                    [
                        "barcode" => "4062345067851",
                        "price" => 37800,
                        "cost" => 37800,
                        "tax_perc" => 20,
                        "tax_amt" => 63000,
                        "quantity" => 1,
                        "tracking_number" => "1Z05V36Y7951053268",
                        "canceled" => "N",
                        "shipped_status_sku" => "not sent"
                    ]
                ],
                "orderId" => "PP0400104913",
                "phone" => "+79620230303",
                "shipping_status" => "not sent",
                "shipping_price" => 1000,
                "shipping_payment_status" => "paid",
                "payment_status" => "paid",
                "created_at" => date('Y-m-d H:i:s')
            ],
            [
                "orderItems" => [
                    [
                        "barcode" => "4062345021658",
                        "price" => 24300,
                        "cost" => 24300,
                        "tax_perc" => 20,
                        "tax_amt" => 4050,
                        "quantity" => 1,
                        "tracking_number" => "1Z05V36Y7951053268",
                        "canceled" => "N",
                        "shipped_status_sku" => "not sent"
                    ],
                    [
                        "barcode" => "4062345067851",
                        "price" => 37800,
                        "cost" => 37800,
                        "tax_perc" => 20,
                        "tax_amt" => 63000,
                        "quantity" => 1,
                        "tracking_number" => "1Z05V36Y7951053268",
                        "canceled" => "N",
                        "shipped_status_sku" => "not sent"
                    ]
                ],
                "orderId" => "PP040023123",
                "phone" => "+79620230303",
                "shipping_status" => "not sent",
                "shipping_price" => 1000,
                "shipping_payment_status" => "not paid",
                "payment_status" => "not paid",
                "created_at" => date('Y-m-d H:i:s')
            ]
        ];
    }
}
