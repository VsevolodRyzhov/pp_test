<?php

namespace App\Controller;

use App\Entity\ShopOrder;
use App\Repository\ShopOrderRepository;
use App\Service\OrderStorage;
use App\Service\OrderSynchronizerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    private $logger;
    private $order_storage;

    public function __construct(LoggerInterface $logger, OrderStorage $order_storage)
    {
        $this->logger = $logger;
        $this->order_storage = $order_storage;
    }

    /**
     * @Route("/", name="home")
     */
    public function index(
        OrderSynchronizerInterface $synchronizer,
        ShopOrderRepository $repository
    )
    {
        $list = $synchronizer->getOrderList(20, date('Y-m-d'), date('Y-m-d'));

        if (!empty($list)) {
            $this->order_storage->store($list);
        }

        $current_date = new \DateTimeImmutable();
        $order_list = $repository->findByDate($current_date);

        return $this->render('home/index.html.twig', [
            'current_date' => $current_date->format('d.m.Y'),
            'order_list' => $order_list
        ]);
    }

    /**
     * @Route("/order/{id}", name="order")
     */
    public function order(ShopOrder $order)
    {
        // here we can call __getOrder__ method from OrderSynchronizerInterface, but we already have all information in local DB from __orders_search__ API method
        return $this->render('home/order.html.twig', [
            'order' => $order
        ]);
    }
}
