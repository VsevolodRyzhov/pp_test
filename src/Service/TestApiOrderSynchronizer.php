<?php


namespace App\Service;

use App\Model\DTO\Product;
use App\Model\DTO\ShopOrder;
use App\Model\DTO\ShopOrderProduct;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class TestApiOrderSynchronizer implements OrderSynchronizerInterface
{
    const URL_LIST_SUFFIX = 'orders_search';
    const URL_ITEM_SUFFIX = 'orders';

    /**
     * @var HttpClientInterface
     */
    private $http_client;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $api_url;

    public function __construct(HttpClientInterface $client, LoggerInterface $logger, $api_url = '/api/')
    {
        $this->http_client = $client;
        $this->logger = $logger;
        // this is hardcode for API testing
        // change this to real IP if you want to update orders from console
        $this->api_url = 'http://' . $_SERVER['SERVER_ADDR'] . $api_url;
    }

    /**
     * @param $count
     * @param null $date_from
     * @param null $date_to
     * @return iterable|null
     */
    public function getOrderList($count, $date_from = null, $date_to = null): ?iterable
    {
        $url = $this->api_url . self::URL_LIST_SUFFIX;
        $items = [];
        try {
            $response = $this->http_client->request('POST', $url, [
                'body' => [
                    'count' => $count,
                    'date_from' => $date_from,
                    'date_to' => $date_to
                ]
            ]);
        } catch (TransportExceptionInterface $e) {
            $this->logger->error($e->getMessage());
        }

        try {
            if ($response->getStatusCode() == 200) {
                $content = json_decode($response->getContent());
                $items = [];
                foreach ($content as $orderItem) {
                    $order = $this->makeOrderDTO($orderItem);
                    foreach ($orderItem->orderItems as $productItem) {
                        $product = $this->makeProductDTO($productItem);
                        $shopOrderProduct = $this->makeShopOrderProductDTO($productItem);
                        $shopOrderProduct->product = $product;
                        $shopOrderProduct->order = $order;
                        $order->addProduct($shopOrderProduct);
                    }
                    $items[] = $order;
                }
            }
        } catch (ClientExceptionInterface $e) {
            $this->logger->error($e->getMessage());
        } catch (RedirectionExceptionInterface $e) {
            $this->logger->error($e->getMessage());
        } catch (ServerExceptionInterface $e) {
            $this->logger->error($e->getMessage());
        } catch (TransportExceptionInterface $e) {
            $this->logger->error($e->getMessage());
        }

        return $items;
    }

    /**
     * @param $orderItem
     * @return ShopOrder
     */
    private function makeOrderDTO($orderItem): ShopOrder
    {
        return new ShopOrder(
            $orderItem->orderId,
            $orderItem->phone,
            $orderItem->shipping_status,
            $orderItem->shipping_price,
            $orderItem->shipping_payment_status,
            $orderItem->payment_status,
            $orderItem->created_at
        );
    }

    /**
     * @param $productItem
     * @return Product
     */
    private function makeProductDTO($productItem): Product
    {
        return new Product($productItem->barcode, $productItem->price);
    }

    private function makeShopOrderProductDTO($item): ShopOrderProduct
    {
        return new ShopOrderProduct(
            $item->cost,
            $item->tax_perc,
            $item->tax_amt,
            $item->quantity,
            $item->tracking_number,
            $item->canceled,
            $item->shipped_status_sku
        );
    }

    /**
     * @inheritDoc
     */
    public function getOrder($id): ?ShopOrder
    {
        // we get all information about order in __orders_search__ API method, so there is no need to implement this function now
        return null;
    }
}