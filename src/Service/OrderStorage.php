<?php


namespace App\Service;


use App\Repository\ProductRepository;
use App\Repository\ShopOrderProductRepository;
use App\Repository\ShopOrderRepository;
use Doctrine\ORM\EntityManagerInterface;

class OrderStorage
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ShopOrderRepository $repository
     */
    private $shop_order_repository;

    /**
     * @var ProductRepository $product_repository
     */
    private $product_repository;

    /**
     * @var ShopOrderProductRepository
     */
    private $shop_order_product_repository;

    public function __construct(EntityManagerInterface $em, ShopOrderRepository $shop_order_repository, ProductRepository $product_repository, ShopOrderProductRepository $shop_order_product_repository)
    {
        $this->em = $em;
        $this->shop_order_repository = $shop_order_repository;
        $this->product_repository = $product_repository;
        $this->shop_order_product_repository = $shop_order_product_repository;
    }

    private function createShopOrder(\App\Model\DTO\ShopOrder $item): \App\Entity\ShopOrder
    {
        $order = $this->shop_order_repository->findOneBy(['order_id' => $item->orderId]);
        if (!$order) {
            $order = new \App\Entity\ShopOrder();
        }
        $order->setOrderId($item->orderId);
        $order->setPhone($item->phone);
        $order->setShippingStatus($item->shipping_status);
        $order->setShippingPrice($item->shipping_price);
        $order->setShippingPaymentStatus($item->shipping_payment_status);
        $order->setPaymentStatus($item->payment_status);
        $order->setCreatedAt(new \DateTimeImmutable($item->created_at));

        return $order;
    }

    private function createShopOrderProduct(\App\Model\DTO\ShopOrderProduct $item, \App\Entity\ShopOrder $order, \App\Entity\Product $product): \App\Entity\ShopOrderProduct
    {
        $shopOrderProduct = $this->shop_order_product_repository->findOneBy([
            'shop_order' => $order->getId(),
            'product' => $product->getId()
        ]);
        if (!$shopOrderProduct) {
            $shopOrderProduct = new \App\Entity\ShopOrderProduct();
        }

        $shopOrderProduct->setTaxPerc($item->tax_perc);
        $shopOrderProduct->setQuantity($item->quantity);
        $shopOrderProduct->setTrackingNumber($item->tracking_number);
        $shopOrderProduct->setCanceled($item->canceled);
        $shopOrderProduct->setCost($item->cost);
        $shopOrderProduct->setTaxAmt($item->tax_amt);
        $shopOrderProduct->setShippedStatusSku($item->shipped_status_sku);
        $shopOrderProduct->setShopOrder($order);
        $shopOrderProduct->setProduct($product);

        return $shopOrderProduct;
    }

    private function createProduct(\App\Model\DTO\Product $item): \App\Entity\Product
    {
        $product = $this->product_repository->findOneBy(['barcode' => $item->barcode]);
        if (!$product) {
            $product = new \App\Entity\Product();
        }
        $product->setBarcode($item->barcode);
        $product->setPrice($item->price);

        return $product;
    }

    /**
     * @param \App\Model\DTO\ShopOrder[] $items
     */
    public function store($items)
    {
        foreach ($items as $item)
        {
            /* @var $item \App\Model\DTO\ShopOrder */
            $order = $this->createShopOrder($item);
            foreach ($item->getProducts() as $productItem) {
                $product = $this->createProduct($productItem->product);
                $this->em->persist($product);

                $shopOrderProduct = $this->createShopOrderProduct($productItem, $order, $product);
                $this->em->persist($shopOrderProduct);
            }
            $this->em->persist($order);
            $this->em->flush();
        }
    }
}