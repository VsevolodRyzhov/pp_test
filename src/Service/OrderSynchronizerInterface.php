<?php

namespace App\Service;

use App\Model\DTO\ShopOrder;

/**
 * Get orders information from external source
 * Interface OrderSynchronizerInterface
 * @package App\Service
 */
interface OrderSynchronizerInterface
{
    /**
     * Get order list from one date to another
     * @param $count
     * @param $date_from
     * @param $date_to
     * @return mixed
     */
    public function getOrderList($count, $date_from, $date_to): ?iterable;

    /**
     * Get specific order info
     * @param $id
     * @return mixed
     */
    public function getOrder($id): ?ShopOrder;
}