<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShopOrderProductRepository")
 */
class ShopOrderProduct
{
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Entity\ShopOrder", inversedBy="shop_order_products")
     * @ORM\JoinColumn(name="shop_order_id", referencedColumnName="id", nullable=false)
     */
    protected $shop_order;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="shop_order_products")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
     */
    protected $product;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $cost;

    /**
     * @ORM\Column(type="smallint")
     */
    private $tax_perc;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $tax_amt;

    /**
     * @ORM\Column(type="smallint")
     */
    private $quantity;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tracking_number;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $canceled;

    /**
     * @ORM\Column(type="string", length=16)
     */
    private $shipped_status_sku;

    public function getShopOrder(): ?ShopOrder
    {
        return $this->shop_order;
    }

    public function setShopOrder(?ShopOrder $shop_order): self
    {
        $this->shop_order = $shop_order;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getCost(): ?string
    {
        return $this->cost;
    }

    public function setCost(string $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getTaxPerc(): ?int
    {
        return $this->tax_perc;
    }

    public function setTaxPerc(int $tax_perc): self
    {
        $this->tax_perc = $tax_perc;

        return $this;
    }

    public function getTaxAmt(): ?string
    {
        return $this->tax_amt;
    }

    public function setTaxAmt(string $tax_amt): self
    {
        $this->tax_amt = $tax_amt;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getTrackingNumber(): ?string
    {
        return $this->tracking_number;
    }

    public function setTrackingNumber(string $tracking_number): self
    {
        $this->tracking_number = $tracking_number;

        return $this;
    }

    public function getCanceled(): ?string
    {
        return $this->canceled;
    }

    public function setCanceled(string $canceled): self
    {
        $this->canceled = $canceled;

        return $this;
    }

    public function getShippedStatusSku(): ?string
    {
        return $this->shipped_status_sku;
    }

    public function setShippedStatusSku(string $shipped_status_sku): self
    {
        $this->shipped_status_sku = $shipped_status_sku;

        return $this;
    }
}
