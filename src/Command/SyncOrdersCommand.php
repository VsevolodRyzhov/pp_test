<?php

namespace App\Command;

use App\Service\OrderStorage;
use App\Service\OrderSynchronizerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SyncOrdersCommand extends Command
{
    private $synchronizer;
    private $order_storage;

    protected static $defaultName = 'app:sync-orders';

    public function __construct(OrderSynchronizerInterface $synchronizer, OrderStorage $order_storage, string $name = null)
    {
        parent::__construct($name);
        $this->synchronizer = $synchronizer;
        $this->order_storage = $order_storage;
    }

    protected function configure()
    {
        $this->setDescription('Sync orders information')
            ->addUsage("There is one sync class (TestApiOrderSynchronizer) for sync between local DB and API. Change api_url attribute in this class if you want to use console worker.");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $list = $this->synchronizer->getOrderList(20, date('Y-m-d'), date('Y-m-d'));

        if (!empty($list)) {
            $this->order_storage->store($list);
        }

        $io->success('Update success');

        return 0;
    }
}
